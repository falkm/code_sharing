

# find ./raw -iname '*.JPG' -exec convert \{\} -verbose -set filename:base "%[basename]" -resize 1560x1040 -quality 80% \> "sorting/%[filename:base].JPG" \;


import sys as SYS
import os as OS
import numpy as NP
import pandas as PD
import atexit as EXIT
from tqdm import tqdm

try:
    import tkinter as TK
    import tkinter.messagebox as TKM
    import tkinter.filedialog as TKF
except ImportError:
    import Tkinter as TK
    import tkMessageBox as TKM
    import tkFileDialog as TKF

import matplotlib as MP
MP.use("TkAgg")
import matplotlib.pyplot as MPP
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk

import skimage.io as SKIO
import skimage.transform as SKIT

#### user input ####

## what are the foto categories?
foto_categories = {    '1': 'archiv' \
                            , '2': 'familienalbum' \
                            , '3': 'pixelfed' \
                            , '4': 'unklar' \
                            , '9': 'loeschen' \
                            }



## file loading
start_at_number = 0


#### The GUI Construct ####
"""
based on example by Thorsten Kranz
http://stackoverflow.com/questions/14336807/matplotlib-event-and-replotting
"""
class QuickLabel_GUI(TK.Tk):
    def __init__(self, start_at_number = 0, in_folder = '.', *args, **kwargs):
        TK.Tk.__init__(self, *args, **kwargs)

        self.alive = True
        EXIT.register(self.Quit)

        self.title("Foto classifier")
        self.pointer = 0
        
        self.filelist = list(sorted([OS.sep.join([in_folder, fi]) for fi in OS.listdir(in_folder) if OS.path.splitext(fi)[-1].lower() in ['.jpg']]))
        self.n_labels = len(self.filelist)
        self.labels = PD.DataFrame.from_dict({'foto': self.filelist})
        self.labels['label'] = ''
        self.labels.index.name = 'idx'

        self.images = []
        for fi in tqdm(self.filelist):
            img = SKIO.imread(fi)
            # img = SKIT.resize(img, (img.shape[0] // 4, img.shape[1] // 4),
            #            anti_aliasing=True)
            self.images.append(img)


        self.savefile = None

        # initialize figure and gui
        self.MakeFigure()
        self.MakeGUI()

        self.Update()
        self.JumpToLabel(start_at_number)

#______________________________________________________________________
### call type assignment
    def AssignCategory(self, fcat):
        self[self.pointer] = foto_categories[fcat]
        self.JumpToLabel(self.pointer + 1)

    def NOOP(self):
        # if unassigned key is pressed
        pass

    def __setitem__(self, pt, label):
        self.labels.loc[pt,'label'] = label

    def __getitem__(self, pt):
        return self.labels.loc[pt,'label']

#______________________________________________________________________
### moving in the data
    def JumpToLabel(self, to_label):
        self.pointer = min([self.n_labels-1, max([0,to_label])])
        self.Update()

    def Previous(self):
        self.JumpToLabel(self.pointer - 1)

    def Next(self):
        self.JumpToLabel(self.pointer + 1)

    def Update(self):
        self.indicator.set("%s/%s" % (self.pointer+1, self.n_labels))
        self.fig.suptitle(self[self.pointer], color = (0.4,0.8,0.4))

        self.ax.clear()

        # img = MPP.imread(self.filelist[self.pointer])[:, :, :]
        self.ax.imshow(\
              self.images[self.pointer] \
            , aspect = 'equal' \
            , interpolation = None \
            )


        self.ax.spines[:].set_visible(False)
        self.ax.get_xaxis().set_visible(False)
        self.ax.get_yaxis().set_visible(False)

        self._canvas.draw()

#______________________________________________________________________
### callbacks
    def PressKey(self,event):
        # print('you pressed', event.key, event.xdata, event.ydata)
        if event.key is None:
            return
        if event.key in foto_categories.keys():
            self.AssignCategory(event.key)

        self.keyboard_shortcuts.get(event.key, self.NOOP )()

    def ReleaseKey(self,event):
        pass
        # print('you pressed', event.key, event.xdata, event.ydata)

        

    def Save(self, evt = None):
        if self.savefile is None:
            self.savefile = TKF.asksaveasfilename( \
                                              defaultextension = ".csv" \
                                            , filetypes = [('comma separated values', '.csv'), ('all files', '.*')] \
                                            , title = 'Select a file to save the labels' \
                                            , initialdir = '.' \
                                            , initialfile = "labels.csv" \
                                            )
            if len(self.savefile) == 0:
                return
        

        # print (self.labels.columns)
        self.labels.to_csv( \
                              self.savefile \
                            , sep = ';' \
                            , header = True \
                            , float_format = '%.4f' \
                            , index = False \
                            )
        print(f"saved! {self.savefile}")




#______________________________________________________________________
### extra special gui creation
    def MakeGUI(self):

        # http://matplotlib.org/users/event_handling.html
        # self.keyboard_shortcuts = {key: self.AssignCallType for key in foto_categories.keys() }
        self.keyboard_shortcuts = {}
        self.keyboard_shortcuts['left'] = self.Previous
        self.keyboard_shortcuts['s'] = self.Save
        self.keyboard_shortcuts['right'] = self.Next
        self.keyboard_shortcuts['escape'] = self.Quit

        ### remove overlapping shortcut keys from matplotlib toolbar
        for command in [cmd for cmd in MPP.rcParams.keys() if 'keymap' in cmd]:
            for shortkey in MPP.rcParams[command]:
                if (shortkey in self.keyboard_shortcuts) or (shortkey in foto_categories.keys()):
                    MPP.rcParams[command].remove(shortkey)
                    # print (command, shortkey)


        buttons_frame = TK.Frame(self)
        buttons_frame.pack(side = TK.TOP, fill=TK.X, expand=False)
        buttons_class = []
        for ct in sorted(foto_categories.keys()):
            buttons_class.append(TK.Button(master = buttons_frame \
                                            , text = "%s (%s)" % (foto_categories[ct], ct) \
                                           , command = lambda x = ct: self.AssignCategory(x))\
                                )
            buttons_class[-1].pack(side = TK.LEFT)
        button_quit = TK.Button(master = buttons_frame, text='Quit', command=self.Quit)
        button_quit.pack(side = TK.RIGHT) #.grid(row=0,column=0)

        button_load = TK.Button(master = buttons_frame, text='Load', command=self.Load)
        button_load.pack(side = TK.RIGHT) #.grid(row=0,column=0)
        button_save = TK.Button(master = buttons_frame, text='Save', command=self.Save)
        button_save.pack(side = TK.RIGHT) #.grid(row=0,column=0)

        self.indicator = TK.StringVar()
        TK.Label(master = buttons_frame, textvariable = self.indicator).pack(side = TK.RIGHT)

        self.indicator.set(f"{self.pointer+1}/{self.n_labels}")


        self._canvas = FigureCanvasTkAgg(self.fig, master = self)
        self._canvas.get_tk_widget().pack(side = TK.TOP, fill = TK.BOTH, expand = 1) #.grid(row=0, column=1, rowspan=3) #
        self._canvas.draw()
        self._canvas.mpl_connect('key_press_event', self.PressKey)
        self._canvas.mpl_connect('key_release_event', self.ReleaseKey)

        toolbar = NavigationToolbar2Tk( self.fig.canvas, self )
        toolbar.pack(side = TK.TOP, fill=TK.Y, expand=0) #.grid(row=3, column=1) #
        toolbar.update()



    def MakeFigure(self):
    # set figure size
        # to get to centimeters, the value is converted to inch (/2.54) 
        #                        and multiplied with empirical factor (*1.25).
        figwidth = 12 #cm
        figheight = 8 #cm

    # define figure
        self.fig = MPP.figure( \
                                  figsize = (figwidth/2.54*1.25, figheight/2.54*1.25) \
                                , facecolor = None \
                                , dpi = 150 \
                                )
        # self.fig.hold(True) #(if multiple figures)

        # MPP.ion() # "interactive mode". Might be useful here, but i don't know. Try to turn it off later.

    # define axis spacing
        self.fig.subplots_adjust( \
                                  top = 1. \
                                , right = 1. \
                                , bottom = 0. \
                                , left = 0. \
                                , wspace = 0. \
                                , hspace = 0. \
                                )

    # # a supertitle for the figure; relevant if multiple subplots
    #     self.fig.suptitle( r"not my $%i^{st}$ plot" % ( 1 ) )

    # get an axis
        self.ax = self.fig.add_subplot(111)


    # axis cosmetics
        self.ax.get_xaxis().set_visible(False)
        self.ax.get_yaxis().set_visible(False)

        self.ax.spines[:].set_visible(False)

### data io
    def Load(self):
        labelfile = TKF.askopenfilename(
                                         defaultextension = ".csv" \
                                            , filetypes = [('comma separated values', '.csv'), ('all files', '.*')] \
                                            , title = 'Select a file to load labels from' \
                                            , initialdir = '.' \
                                         )
        loaded_labels = PD.read_csv( \
                                labelfile \
                            , sep = ';' \
                            , header = 0 \
                            ) #.set_index('foto', inplace = False)


        # index_translator = {row['foto']: idx for idx, row in self.labels.iterrows()}
        # revisited_labels = [(index_translator[foto], foto) for foto in loaded_labels.index.values if index_translator[foto] in self.labels.index.values]
        # self.labels.loc[[rv[0] for rv in revisited_labels], :] = loaded_labels.loc[[rv[1] for rv in revisited_labels], :]


        current_fotos = self.labels['foto'].values
        print (current_fotos)
        for idx, row in loaded_labels.iterrows():
            if row['foto'] not in current_fotos:
                continue

            self.labels.loc[self.labels['foto'].values == row['foto'], row.index.values] = row.values


        # if loaded_labels.shape[0] == self.n_labels:
        #     self.labels = loaded_labels
        # else:
        #     print ("files don't match!")

        print (self.labels.sample(5))

#______________________________________________________________________
### exit
    def Quit(self):
        if self.alive:
            if TKM.askyesno("please don't go...",'really quit?'):
                self.alive = False
                MPP.close(self.fig)
                SYS.exit()

 

"""
#######################################################################
### Mission Control                                                 ###
#######################################################################
"""
if __name__ == "__main__":
    # basepath, filelist = userDefinedFilename()

    # global start_at_number
    start_at_number = 450

    gui = QuickLabel_GUI(start_at_number, in_folder = 'sorting')
    gui.mainloop()
    # MPP.show()



