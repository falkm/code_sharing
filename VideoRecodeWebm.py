#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
A video conversion tool
to convert large, unencoded raw videos from consumer cameras
to VP9 encoded webm files
of a given quality
which will save hard drive memory.
Also exposes video creation time to filename.

_ example usage _
# To convert all videos in the present folder:
    python VideoRecodeWebm.py 

# dedicated folders for raw, archive, target
python VideoRecodeWebm.py raw_videos archive converted_videos


This is my first piece of program created in the VIM editor.
"""


# library imports
import os as OS # operating system control
import sys as SYS # system commands, to get command line arguments
import time as TI # for a small pause after conversion
import datetime as DT # format file modification date
from tqdm import tqdm # loops with progress status 

# list of converted extensions
extension_list = [".mp4", ".mov"]

# get all relevant files from a specified folder
def FindAllFiles(directory = "."):
    # create a list of files using the OS.listdir command
    # but only for certain extensions
    # and with the full (relative) path
    # sorting the outcome alphabetically
    filelist = list(sorted([\
                            OS.sep.join([directory, fi]) for fi in OS.listdir(directory) \
                            if OS.path.splitext(fi)[-1].lower() in extension_list\
                            ])) 

    # return that filelist
    return filelist

# convert files to "webm", vp9 codec, 2-pass encoding
# https://trac.ffmpeg.org/wiki/Encode/VP9
def ConvertToWebM(filelist, out_folder = None, archive_folder = f'.{OS.sep}archive'):

    # move raw files to archive
    # check if the archive folder already exists
    if archive_folder is None:
        archive_folder = f""".{OS.sep}archive_{DT.date.today().strftime("%Y%m%d")}"""

    # ... otherwise create it
    if not OS.path.isdir(archive_folder):
        OS.mkdir(archive_folder)


    # prepare a universal conversion command
    conversion_command_1 = """ ffmpeg -i {fi:s} -c:v libvpx-vp9 -pass 1 -crf {crf:.0f} -an -f null -loglevel 0 /dev/null """ \
                       + """&& ffmpeg -y -i {fi:s} -c:v libvpx-vp9 -pass 2 -crf {crf:.0f} -c:a libopus -loglevel 0 {fo:s}"""
    conversion_command_2 = """ ffmpeg -i {fi:s} -vf scale=-1:600 -crf {crf:.0f} -loglevel 0 {fo:s} """ 

    # quality parameter
    crf = 20

    # loop through the filelist
    for fi in tqdm(filelist):

        # get the output file by replacing the file extension
        fo = fi.replace(OS.path.splitext(fi)[-1], '.webm')
        
        # prepend timestamp
        file_time = DT.datetime.fromtimestamp(OS.path.getmtime(fi)).strftime("%Y%m%d")
        fo = OS.sep.join(list(OS.path.split(fo)[:-1]) + ["%s_%s" % (file_time, OS.path.split(fo)[-1])])

        # optionally: put file to different output folder
        if out_folder is not None:
            fo = OS.sep.join([out_folder, OS.path.split(fo)[-1]])

        # execute conversion command, filling in the text formatters
        # if not ('DSCF1815' in fi):
        OS.system(conversion_command_1.format(fi=fi, fo=fo, crf = crf))
        TI.sleep(1.) # to allow keyboard interrupt

        # generate low quality preview
        fo2 = fo.replace('.webm', '_preview.webm')
        OS.system(conversion_command_2.format(fi=fo, fo=fo2, crf = 32))
        TI.sleep(1.) # to allow keyboard interrupt

        # move raw video files to archive folder
        OS.system(f"mv {fi} {archive_folder}{OS.sep}")



# mission control
if __name__ == """__main__""":
    # read system input
    args = SYS.argv

    # select an input folder from user input_folder
    input_folder = '.'
    output_folder = None
    archive_folder = None

    if len(args) > 1:
        # user specified an input folder
        input_folder = args[1]

    if len(args) > 2:
        # user specified an archive folder
        archive_folder = args[2]
 
    if len(args) > 3:
        # user specified an output folder
        output_folder = args[3]
    
    # retrieve a list of files
    filelist = FindAllFiles(input_folder)

    print (f'converting {len(filelist)} files:')
    # convert
    ConvertToWebM(  filelist \
                  , out_folder = output_folder \
                  , archive_folder = archive_folder\
                  )

    print ('done!')


# Done!
